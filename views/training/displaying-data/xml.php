<?php
/**
 * Created by PhpStorm.
 * User: Kruglyak
 * Date: 04.04.2019
 * Time: 22:25
 */

 echo \yii\grid\GridView::widget([
    'dataProvider' => $provider,
     'filterModel' => $searchModel,
     'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
         [
             "attribute" => "name",
             'filter' => '<input class="form-control" name="name" value="'. $searchModel['name'] .'" type="text">',
             'value' => 'name',
         ],
        'price',
        'hidden',
        'qwer',

    ],
]);

?>
