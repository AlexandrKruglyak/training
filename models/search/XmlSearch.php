<?php

namespace app\models\search;

use yii\data\ArrayDataProvider;
use yii;

class XmlSearch
{

    public function filterName($item) :bool
    {
        $name = Yii::$app->request->getQueryParam('name', '');

        if (strlen($name) > 0) {
            if (strpos(strtolower($item['name']), strtolower($name)) !== false) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public function filterPrice($item) :bool
    {
        $name = Yii::$app->request->getQueryParam('price', '');
        if (strlen($name) > 0) {
            if (strpos($item['price'], $name) !== false) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    private function convertXmlToArray($arrObjData, $arrSkipIndices = array()) :array
    {
        $arrData = array();

        // if input is object, convert into array
        if (is_object($arrObjData)) {
            $arrObjData = get_object_vars($arrObjData);
        }

        if (is_array($arrObjData)) {
            foreach ($arrObjData as $index => $value) {
                if (is_object($value) || is_array($value)) {
                    $value = $this->convertXmlToArray($value, $arrSkipIndices); // recursive call
                }
                if (in_array($index, $arrSkipIndices)) {
                    continue;
                }
                $arrData[$index] = $value;
            }
        }
        return $arrData;
    }

    private function getRelationArray($categories, $products) :array
    {
        $results = [];
        $sortCat = [];
        foreach ($categories['item'] as $category){
            $sortCat[$category['id']] = ['name'=>$category['name']];
        }
        foreach ($products['item'] as $product){
            $results[] = ['price'=>$product['price'], 'id'=>$product['id'], 'name'=>$sortCat[$product['categoryId']]['name'],'hidden'=>$product['hidden']];
        }

        return $results;
    }


    public function getArrayXml($path){
        $xmlStr = file_get_contents($path);
        $xmlObj = simplexml_load_string($xmlStr);
        $results = $this->convertXmlToArray($xmlObj);
        return $results;
    }


    public function provider()
    {
        $xmlProd = yii::getAlias('@web').'xml/products.xml';
        $xmlCat = yii::getAlias('@web').'xml/categories.xml';

        $arrayProducts = $this->getArrayXml($xmlProd);
        $arrayCat = $this->getArrayXml($xmlCat);

        $data = $this->getRelationArray($arrayCat,$arrayProducts);

        $filteredresultData = array_filter($data, [$this, 'filterName']);

        $dataProvider = new \yii\data\ArrayDataProvider([
            'key'=>'id',
            'allModels' => $filteredresultData,
            'sort' => [
                'attributes' => ['id', 'name', 'price','hidden'],
            ],
        ]);

        return $dataProvider;
    }
}
