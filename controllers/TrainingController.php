<?php

namespace app\controllers;

use app\models\search\XmlSearch;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii;

class TrainingController extends Controller
{

    public function actionDisplayingDataFormatter()
    {
        return $this->render('displaying-data/formatter');
    }

    public function actionDisplayingDataXml()
    {

        $name = yii::$app->request->getQueryParam('name', '');
        $price = Yii::$app->request->getQueryParam('price', '');
        $searchModel = new XmlSearch();
        $provider = $searchModel->provider();
        $searchModel = ['id' => null, 'name' => $name, 'price' => $price, 'hidden'];
        return $this->render('displaying-data/xml', ['provider'=>$provider, 'searchModel'=>$searchModel]);
    }

    public function actionHelperTruncate()
    {
        return $this->render('helpers/truncate');
    }

    public function actionHelperCamelize()
    {
        return $this->render('helpers/camelize');
    }

    public function actionHelperTransliterate()
    {
        return $this->render('helpers/transliterate');
    }


}
